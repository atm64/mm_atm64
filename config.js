/* MagicMirror² Config Sample
 *
 * By Michael Teeuw https://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information on how you can configure this file
 * see https://docs.magicmirror.builders/configuration/introduction.html
 * and https://docs.magicmirror.builders/modules/configuration.html
 *
 * You can use environment variables using a `config.js.template` file instead of `config.js`
 * which will be converted to `config.js` while starting. For more information
 * see https://docs.magicmirror.builders/configuration/introduction.html#enviromnent-variables
 */
let config = {
	address: "localhost",	// Address to listen on, can be:
							// - "localhost", "127.0.0.1", "::1" to listen on loopback interface
							// - another specific IPv4/6 to listen on a specific interface
							// - "0.0.0.0", "::" to listen on any interface
							// Default, when address config is left out or empty, is "localhost"
	port: 8080,
	basePath: "/",			// The URL path where MagicMirror² is hosted. If you are using a Reverse proxy
					  		// you must set the sub path here. basePath must end with a /
	ipWhitelist: ["127.0.0.1", "::ffff:127.0.0.1", "::1"],	// Set [] to allow all IP addresses
															// or add a specific IPv4 of 192.168.1.5 :
															// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
															// or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
															// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	useHttps: false, 		// Support HTTPS or not, default "false" will use HTTP
	httpsPrivateKey: "", 	// HTTPS private key path, only require when useHttps is true
	httpsCertificate: "", 	// HTTPS Certificate path, only require when useHttps is true

	language: "fr",
	locale: "fr-FR",
	logLevel: ["INFO", "LOG", "WARN", "ERROR"], // Add "DEBUG" for even more logging
	timeFormat: 24,
	units: "metric",

	modules: [
		{
			module: "alert",
		},
		{
			module: "updatenotification",
			position: "top_bar"
		},
		{
			module: "clock",
			position: "top_left",
				config:{
					showWeek: true
				}

		},
		{
			module: "calendar",
			header: "Vacances scolaires",
			position: "top_left",
			config: {
				calendars: [
					{
						symbol: "calendar-check",
						url: "https://fr.ftp.opendatasoft.com/openscol/fr-en-calendrier-scolaire/Zone-C.ics"
					}
				]
			}
		},
		{
			module: "compliments",
			position: "lower_third",
			config: {
				compliments: {
					"anytime" : [
						"ATM64\nBienvenue !",
						"ATM64\nAtelier de Téléphonie Mobile\nSalies de Béarn",
						"ATM64\nRéparation, reconditionnement, rachat d'appareils",
						"En donnant une seconde vie à nos appareils, nous faisons un geste pour l'environnement",
						"Réparer permet de réduire la production de déchets électroniques et plastiques",
						"Réparer évite de mobiliser des ressources pour la fabrication et le transport d'un appareil neuf",
						"Réparer aide à lutter contre l'obsolescence matérielle"
					]
				}
			}

		},
		{
			module: "weather",
			position: "top_right",
			config: {
				weatherProvider: "openweathermap",
				type: "current",
				location: "Salies-de-Béarn",
				locationID: "2976444", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				apiKey: "27cdb5d6ddde1eaef213aa8135ceb274"
			}
		},
		{
			module: "weather",
			position: "top_right",
			header: "Prévisions",
			config: {
				weatherProvider: "openweathermap",
				type: "forecast",
				location: "Salies-de-Béarn",
				locationID: "2976444", //ID from http://bulk.openweathermap.org/sample/city.list.json.gz; unzip the gz file and find your city
				apiKey: "27cdb5d6ddde1eaef213aa8135ceb274"
			}
		},
		{
			module: "newsfeed",
			position: "bottom_bar",
			config: {
				feeds: [
					{
						title: "Sud-Ouest / Pau",
						url: "https://www.sudouest.fr/pyrenees-atlantiques/pau/rss.xml"
					},
					{
						title: "Sud-Ouest / Biarritz",
						url: "https://www.sudouest.fr/pyrenees-atlantiques/biarritz/rss.xml"
					},
					{
						title: "Sud-Ouest / Bayonne",
						url: "https://www.sudouest.fr/pyrenees-atlantiques/bayonne/rss.xml"
					},
					{
						title: "Sud-Ouest / Mont-De-Marsan",
						url: "https://www.sudouest.fr/landes/mont-de-marsan/rss.xml"
					}

				],
				showSourceTitle: true,
				showPublishDate: true,
				broadcastNewsFeeds: true,
				broadcastNewsUpdates: true
			}
		},
	]
};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") {module.exports = config;}
